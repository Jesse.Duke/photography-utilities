# Script to rename file based on creation date.
#  - Results = %H.%M.%S_%Y-%m-%d+index

import sys, os, datetime

directory = './'
extensions = (['.m4v', '.mp4', '.mov', '.3gp']);

filelist = os.listdir( directory )

newfilesDictionary = {}

count = 0

for file in filelist:
    filename, extension = os.path.splitext(file)
    if ( extension in extensions ):
        # create_time = os.path.getctime( file )
        create_time = os.stat(file).st_birthtime
        format_time = datetime.datetime.fromtimestamp( create_time )
        format_time_string = format_time.strftime("%H.%M.%S_%Y-%m-%d")
        newfile = format_time_string + extension; 

        if ( newfile in newfilesDictionary.keys() ):
            index = newfilesDictionary[newfile] + 1;
            newfilesDictionary[newfile] = index; 
            newfile = format_time_string + '-' + str(index) + extension;
        else:
            newfilesDictionary[newfile] = 0; 

        os.rename( file, newfile );
        count = count + 1
        print( file.rjust(35) + '    =>    ' + newfile.ljust(35) )


print( 'All done. ' + str(count) + ' files are renamed. ')
