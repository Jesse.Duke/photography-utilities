#!/bin/bash 
# Script to modify time stamp based on base date and suffix number
# set -x

# Set the target date
target_date="01/19/2025"

# Set the base time
base_time="12:00:00"

# Set the directory to process (change this if needed)
directory="."  # Current directory

# Loop through all files in the directory
find "$directory" -maxdepth 1 -type f -name "*-*" -print0 | while IFS= read -r -d $'\0' file; do
  # Extract the suffix (sequence number) using extended globbing
  filename=$(basename "$file")
  if [[ "$filename" == *-*[0-9]* ]]; then # Check if the filename matches the pattern
    suffix="${filename##*-}" # Remove everything up to and including the last dash
    suffix="${suffix%%.*}"  # Remove everything after the first dot (file extension)

    # Check if a suffix was found and is numeric
    if [[ -n "${suffix}" && "${suffix}" =~ ^[0-9]+$ ]]; then
      # Convert the suffix to an integer
      suffix="${suffix##+(0)}"
      seconds=$(printf "%d" "$((10#$suffix))")

      # Calculate the new time
      new_time=$(date -j -f "%H:%M:%S" "${base_time}" +%s)
      new_time=$((new_time + seconds))
      new_time=$(date -j -f "%s" "${new_time}" "+%H:%M:%S")

      # Construct the full date and time string
      new_datetime="${target_date} ${new_time}"

      echo "${new_datetime}"

      # Set the modification time using touch
      touch -mt "$(date -j -f "%m/%d/%Y %H:%M:%S" "$new_datetime" "+%Y%m%d%H%M.%S")" "$file"

      echo "Updated modification time for: ${file} to: ${new_datetime}"
      
    else
      echo "Warning: Could not extract a valid numeric suffix from file: ${file}"
    fi
  else
    echo "Warning: Filename does not match expected pattern (no dash): ${file}"
  fi
done

echo "Finished processing files."
# set +x